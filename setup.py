from setuptools import setup

setup(name='Sample-python-package',
      version='0.1',
      description='Test-sample-python-package',
      url='http://github.com/AlecA50/sample-python',
      author='AA',
      author_email='AA@example.com',
      license='MIT',
      packages='app.yaml','server.py', 'deploy.template.yaml',
      zip_safe=False)
